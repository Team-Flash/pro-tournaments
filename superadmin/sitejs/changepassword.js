$(function() { //initial fn starts here

    $(".cpldr").hide();
    //enter click fn signup/login starts here
    $("input").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".cpBtn").click() : "";
    });

}); //initial fn ends here


//change password fn starts here
function changepassword() {

    if ($('#oldpwd').val().trim() == '') {
        $("#oldpwd").addClass("iserr");
        $("#snackbarerror").text("Old Password is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#oldpwd').val().length < 6) {
        $("#oldpwd").addClass("iserr");
        $("#snackbarerror").text("Atleast 6 characters is required for old password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#newpwd').val().trim() == '') {
        $("#newpwd").addClass("iserr");
        $("#snackbarerror").text("New Password is Required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#newpwd').val().length < 6) {
        $("#newpwd").addClass("iserr");
        $("#snackbarerror").text("Atleast 6 characters is required for New password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#oldpwd').val() == $('#newpwd').val()) {
        $("#snackbarerror").text("Old Password & New Password should not be same!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#renewpass').val().trim() == '') {
        $("#renewpass").addClass("iserr");
        $("#snackbarerror").text("Re - New Password is Required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#renewpass').val().length < 6) {
        $("#renewpass").addClass("iserr");
        $("#snackbarerror").text("Atleast 6 characters is required for re - new password");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#newpwd').val() != $('#renewpass').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    $(".cpldr").show();
    $(".cpBtn").attr("disabled", true);
    var postdata = JSON.stringify({
        "old_password": $("#oldpwd").val(),
        "new_password": $("#newpwd").val()
    });
    $.ajax({
        url: changepassword_api,
        type: 'post',
        data: postdata,
        headers: {
            "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".cpldr").hide();
            $(".cpBtn").attr("disabled", false);
        },
        error: function(data) {
            $(".cpldr").hide();
            $(".cpBtn").attr("disabled", false);
            var errtext = '';
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Password Changed Sucessfully!");
        showsuccesstoast();
        setTimeout(function() { window.location.href = "accept-tournaments.html"; }, 3000);
    });
} //change password fn ends here