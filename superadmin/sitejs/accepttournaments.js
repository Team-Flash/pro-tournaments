$(function() {
    $(".nodatarow_1,.nodatarow_1.showallldr,.loadmore_div_1,.loadmore_div_2,.loadmore_ldr_1,.loadmore_ldr_2").hide();
    listdata(1, 1);
    listdata(1, 2);
});

function listdata(type, tabtype) {
    sessionStorage.type = type;
    sessionStorage.tabtype = tabtype;

    var search = sessionStorage.search ? sessionStorage.search : "";

    var senddata = JSON.stringify({
        "search": search,
        "is_active": sessionStorage.tabtype == 1 ? "False" : "True"
    });

    if (type == 1) {
        var url = listtournaments_api;
    } else {
        var url = sessionStorage.next_url;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: senddata,
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            if (type == 1) {
                $(".tbody_" + tabtype).empty();
            }
            $(".table_" + tabtype).show();
            $(".nodatarow_" + tabtype).hide();
            if (tabtype == 1) {
                sessionStorage.listdata_1 = JSON.stringify(data);
            } else {
                sessionStorage.listdata_2 = JSON.stringify(data);
            }
            loadlisting_details(tabtype);
        },
        error: function(data) {
            console.log("error occured in listing page");
            if (type == 1) {
                $(".tbody_" + tabtype).empty();
            }
            $(".table_" + tabtype).hide();
            $(".nodatarow_" + tabtype).show();
            $(".showallldr").hide();
            $(".loadmore_div_" + tabtype).hide();
            $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

function loadlisting_details(tabtype) {

    if (tabtype == 1) {
        var data = JSON.parse(sessionStorage.listdata_1);
    } else {
        var data = JSON.parse(sessionStorage.listdata_2);
    }

    if (data.next_url != null) {
        $(".loadmore_div_" + tabtype).show();
        sessionStorage.next_url = data.next_url;
    } else {
        $(".loadmore_div_" + tabtype).hide();
    }

    if (data.results.length == 0) {
        $(".dynpagination_" + tabtype).empty().hide();
        $(".nodatarow_" + tabtype).show();
        $(".table_" + tabtype).hide();
    } else {
        if (tabtype == 1) {
            for (var i = 0; i < data['results'].length; i++) {
                var startdate = monthNames[parseInt(data['results'][i].start_date.substring(5, 7)) - 1] + " " + data['results'][i].start_date.substring(0, 4) + ", " + data['results'][i].start_date.substring(8, 10);
                var enddate = monthNames[parseInt(data['results'][i].end_date.substring(5, 7)) - 1] + " " + data['results'][i].end_date.substring(0, 4) + ", " + data['results'][i].end_date.substring(8, 10);
                $(".tbody_" + tabtype).append(`<tr>
                                                <td class="text-center">${data['results'][i].id}</td>
                                                <td>${data['results'][i].name}</td>
                                                <td>From ${startdate} to ${enddate}</td>
                                                <td>${data['results'][i].time}</td>
                                                <td>${data['results'][i].max_entries}</td>
                                                <td><span class="status-${data['results'][i].is_updated ? "edit" : "new"}">${data['results'][i].is_updated ? "EDITED" : "NEW"}</span></td>
                                                <td><a onclick="viewtournament(${data['results'][i].id})" class="table-viewmorebtn cptr">View More</a></td>
                                                <td>
                                                    <button class="accept-btn" onclick="accepttournament(${data['results'][i].id})">Accept</button>
                                                </td>
                                            </tr>`);
            } //for loop ends here
        } //if cond ends here
        else {
            for (var i = 0; i < data['results'].length; i++) {
                var startdate = monthNames[parseInt(data['results'][i].start_date.substring(5, 7)) - 1] + " " + data['results'][i].start_date.substring(0, 4) + ", " + data['results'][i].start_date.substring(8, 10);
                var enddate = monthNames[parseInt(data['results'][i].end_date.substring(5, 7)) - 1] + " " + data['results'][i].end_date.substring(0, 4) + ", " + data['results'][i].end_date.substring(8, 10);
                $(".tbody_" + tabtype).append(`<tr>
                                                <td class="text-center">${data['results'][i].id}</td>
                                                <td>${data['results'][i].name}</td>
                                                <td>From ${startdate} to ${enddate}</td>
                                                <td>${data['results'][i].time}</td>
                                                <td>${data['results'][i].max_entries}</td>
                                                <td><a onclick="viewtournament(${data['results'][i].id})" class="table-viewmorebtn cptr">View More</a></td>
                                            </tr>`);
            }

        } //else cond ends here
    } //else cond ends here
    $(".loadmore_ldr_" + tabtype).hide();
    $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
    $(".showallldr").hide();
}

function loadmore(type) {
    listdata(2, type);
    $(".loadmore_ldr_" + type).show();
    $(".loadmore_div_" + type).show();
}

//Scroll Handle...!!!
$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        console.log("scrolld");
        if ($(".loadmore_div_" + $(".tabtop.active").attr('tabid')).is(":visible")) {
            loadmore($(".tabtop.active").attr('tabid'))
        }
    }
});

function searchnow() {
    sessionStorage.search = $("#searchtext").val();
    $(".searchBtn").empty().append(`<i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1, $(".tabtop.active").attr('tabid'));
}

function showall() {
    $("#searchtext").val("");
    sessionStorage.search = "";
    $(".showallldr").show();
    listdata(1, $(".tabtop.active").attr('tabid'));
}

function viewtournament(id) {
    sessionStorage.t_id = id;
    window.location.href = "tournament-detail.html";
}

var acceptid = '';

function accepttournament(id) {
    acceptid = id;
    $('#conformModal').modal('show');
}

function accepttour_final() {
    $(".cp_Btn").html(`Submit &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    $.ajax({
        url: accepttournament_api + acceptid + '/',
        type: 'put',
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".cp_Btn").html(`Submit`).attr("disabled", false);
            $("#conformModal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Accepted successfully!");
            showsuccesstoast();
            listdata(1, 1);
            listdata(1, 2);
        },
        error: function(data) {
            $(".cp_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}