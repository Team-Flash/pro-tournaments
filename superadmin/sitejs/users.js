$(function() {

    $(".nodatarow").hide();
    listdata(1);
    var search = sessionStorage.search ? sessionStorage.search : "";

});

function listdata(type) {
    sessionStorage.type = type;

    var search = sessionStorage.search ? sessionStorage.search : "";

    var senddata = JSON.stringify({
        "search": search
    });

    if (type == 1) {
        var url = listusers_api;
        sno = 0;
    } else {
        var url = listusers_api + "?page=" + sessionStorage.gotopage_no;
        sno = 0;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: senddata,
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            $(".dyn_list").empty();
            $(".dyndataTable").show();
            $(".nodatarow").hide();
            sessionStorage.listdata = JSON.stringify(data);
            loadlisting_details();
        },
        error: function(data) {
            console.log("error occured in listing page");
            $(".dyn_list").empty();
            $(".nodatarow").show();
            $(".dyndataTable").hide();
            $(".showallldr").hide();
            $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
        }
    });
}

function loadlisting_details() {
    var data = JSON.parse(sessionStorage.listdata);
    if (data.results.length == 0) {
        $(".dynpagination").empty().hide();
        // $(".dyn_listing").append(`<br><br><div class="row"><center><img src="nodata.jpg" style="width:40%"><br><br><p class="nolistdata">No Data Found</p></center></div>`)
        $(".nodatarow").show();
        $(".dyndataTable").hide();
    } else {
        if (sessionStorage.type == 1) {
            pagination(data.count);
        }
        for (var i = 0; i < data['results'].length; i++) {
            $(".dyn_list").append(`<tr>
                                    <td class="text-center">${data['results'][i].id}</td>
                                    <td>${data['results'][i].first_name}</td>
                                    <td>${data['results'][i].email ? data['results'][i].email : "N/A"}</td>
                                    <td>${data['results'][i].username}</td>
                                    <td><a onclick="changepass(${data['results'][i].id})" href="#changepwdmodal" data-toggle="modal">Change Password</a></td>
                                </tr>`);

        } //for loop ends here
    } //else cond ends here
    $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
    $(".showallldr").hide();
}

//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li><a onclick="previouspage()" class="cptr"> <i class="ti-arrow-left"></i> </a></li>`);
    if ((count / 20) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / 20);
    } else {
        var totalpagescount = parseInt((count / 20), 10) + 1;
    }
    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li><a onclick="nextpage()" class="cptr"> <i class="ti-arrow-right"></i> </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 3;
    for (var i = 1; i <= 3; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    sessionStorage.whichclick = 2;
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");
    sessionStorage.gotopage_no = pageno;
    listdata(2);
}

function nextpage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

    if ((currentpageno % 3) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
        $(".pageli").hide();
        for (var i = currentpageno; i <= currentpageno + 2; i++) {
            $(".pageli" + i).show();
        }
    }


    currentpageno++;
    console.log(currentpageno);
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);
    }
}

function previouspage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

    if ((currentpageno % 3) == 0) {
        $(".pageli").hide();
        for (var i = currentpageno; i >= currentpageno - 2; i--) {
            $(".pageli" + i).show();
        }
    }

    currentpageno--;
    console.log(currentpageno);
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);

    }
}

function searchnow() {
    sessionStorage.search = $("#searchtext").val();
    $(".searchBtn").empty().append(`<i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listdata(1);
}

function showall() {
    sessionStorage.search = "";
    $(".showallldr").show();
    listdata(1);
}

var categoryid = '';

function changepass(id) {
    categoryid = id;
}

function changepass_final() {

    if ($('#changepass').val() == '') {
        $('#changepass').addClass('iserr');
        $("#snackbarerror").text("Password is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".cp_Btn").html(`Submit &nbsp;<i class="fa fa-spinner fa-spin fa-2x fa-fw btnldr"></i>`).attr("disabled", true);
    var postData = JSON.stringify({
        "password": $("#changepass").val()
    });

    $.ajax({
        url: resetpwdinusers_api + categoryid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        success: function(data) {
            $(".cp_Btn").html(`Submit`).attr("disabled", false);
            $("#changepwdmodal").modal("toggle");
            $("#snackbarsuccs").text("Password has been changed successfully!");
            showsuccesstoast();
            $("#changepass").val("");
        },
        error: function(data) {
            $(".cp_Btn").html(`Submit`).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}