$(function() {
    $(".nodatarow,.showallldr").hide();
     sessionStorage.search = "";
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    listbookings(1);
});

var sno = 0;

function listbookings(type) {
    sessionStorage.type = type;

    var search = sessionStorage.search ? sessionStorage.search : "";
    var fromdate = sessionStorage.fromdate ? sessionStorage.fromdate.replace(/ /g, '') : "";
    var todate = sessionStorage.todate ? sessionStorage.todate.replace(/ /g, '') : "";

    var senddata = JSON.stringify({
        "search": search,
        "from_date": fromdate,
        "to_date": todate
    });

    if (type == 1) {
        var url = listing_api;
        sno = 0;
    } else {
        var url = listing_api + "?page=" + sessionStorage.gotopage_no;
        sno = 0;
    }

    $.ajax({
        url: url,
        type: 'POST',
        data: senddata,
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            $(".dyn_listing").empty();
            $(".dyndataTable").show();
            $(".nodatarow").hide();
            sessionStorage.bookingdata = JSON.stringify(data);
            loadlisting_details();
        },
        error: function(data) {
            console.log("error occured in listing page");
            $(".dyn_listing").empty();
            $(".nodatarow").show();
            $(".dyndataTable").hide();
            $(".showallldr").hide();
            $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
        }
    });
}

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

function loadlisting_details() {
    var data = JSON.parse(sessionStorage.bookingdata);
    if (data.results.length == 0) {
        $(".dynpagination").empty().hide();
        // $(".dyn_listing").append(`<br><br><div class="row"><center><img src="nodata.jpg" style="width:40%"><br><br><p class="nolistdata">No Data Found</p></center></div>`)
        $(".nodatarow").show();
        $(".dyndataTable").hide();
    } else {
        if (sessionStorage.type == 1) {
            pagination(data.count);
        }
        for (var i = 0; i < data['results'].length; i++) {
            sno++;
            var date = monthNames[parseInt(data.results[i].created_on.substring(5, 7)) - 1] + " " + data.results[i].created_on.substring(8, 10) + ", " + data.results[i].created_on.substring(0, 4);
            var time = data.results[i].created_on.slice(11, 16);
            if (Number(time.slice(0, 2)) == 12) {
                time = time + ' PM';
            } else if (Number(time.slice(0, 2)) > 12) {
                time = Number(time.slice(0, 2)) - 12 + time.slice(2, 5) + ' PM';
            } else {
                time = time + ' AM';
            }
            $(".dyn_listing").append(`<tr>
                                    <td class="text-center">${data['results'][i].id}</td>
                                     <td>${data['results'][i].tournament.user.first_name}</td>
                                     <td>${data['results'][i].tournament.user.username}</td>
                                      <td>${data['results'][i].tournament.name}</td>
                                    <td>${data['results'][i].user.first_name}</td>
                                    <td>${data['results'][i].user.username}
                                        <br/><span class="text-muted">${data['results'][i].user.email}</span></td>
                                    <td>${date}
                                        <br/><span class="text-muted">${time}</span></td>
                                    }
                                   
                                </tr>`);
        } //for loop ends here
    } //else cond ends here
    $(".searchBtn").empty().append(`<i class="fa fa-search whiteclr"></i>`);
    $(".showallldr").hide();
}

//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li><a onclick="previouspage()" class="cptr"> <i class="ti-arrow-left"></i> </a></li>`);
    if ((count / 20) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / 20);
    } else {
        var totalpagescount = parseInt((count / 20), 10) + 1;
    }
    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li><a onclick="nextpage()" class="cptr"> <i class="ti-arrow-right"></i> </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 3;
    for (var i = 1; i <= 3; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    sessionStorage.whichclick = 2;
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");
    sessionStorage.gotopage_no = pageno;
    listdata(2);
}

function nextpage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

    if ((currentpageno % 3) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
        $(".pageli").hide();
        for (var i = currentpageno; i <= currentpageno + 2; i++) {
            $(".pageli" + i).show();
        }
    }


    currentpageno++;
    console.log(currentpageno);
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);
    }
}

function previouspage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

    if ((currentpageno % 3) == 0) {
        $(".pageli").hide();
        for (var i = currentpageno; i >= currentpageno - 2; i--) {
            $(".pageli" + i).show();
        }
    }

    currentpageno--;
    console.log(currentpageno);
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");
        sessionStorage.gotopage_no = currentpageno;
        listdata(2);

    }
}

var search = sessionStorage.search ? sessionStorage.search : "";
var fromdate = sessionStorage.fromdate ? sessionStorage.fromdate : "";
var todate = sessionStorage.todate ? sessionStorage.todate : "";
//saerch fn starts here
function searchnow() {
    sessionStorage.search = $("#searchtext").val();
    $(".searchBtn").empty().append(`<i class="fa fa-spinner fa-spin fa-fw btnldr whiteclr"></i>`);
    listbookings(1);
}

function showall() {
    sessionStorage.search = "";
    sessionStorage.fromdate = "";
    sessionStorage.todate = "";
    $(".showallldr").show();
    listbookings(1);
}

$(".applyBtn").click(function() {
    var startdate = $("#searchdate").val().split('-')[0];
    var enddate = $("#searchdate").val().split('-')[1];
    sessionStorage.fromdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
    sessionStorage.todate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
    listbookings(1);
});