$(function() {

    document.getElementById('coverpicture').onchange = function() {
        $(".coverfilename").text(this.value);
        // $("#coverpicmodal").modal("toggle");
    };
    document.getElementById('invitationpicture').onchange = function() {
        $(".invitationfilename").text(this.value);
        // $("#invitationpicmodal").modal("toggle");
    };

    getdetails();

    $(".picuploadLdr_1,.picuploadLdr_2,.t_detailssavldr,.t_details_addr_savldr,.t_details_stadium_savldr,.edit_rules_ldr,.add_rules_ldr,.edit_price_ldr,.add_price_ldr,.edit_org_ldr,.add_org_ldr").hide();

    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
    });

});

function getdetails() {

    $.ajax({
        url: retrievedetails_api + sessionStorage.t_id + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            sessionStorage.t_details = JSON.stringify(data);

            var myElement = document.querySelector(".bannerimageedit");
            myElement.style.backgroundImage = "url(" + data.cover_pic + ")";

            var myElement = document.querySelector(".bannerimage-invitation");
            myElement.style.backgroundImage = "url(" + data.invitation_pic + ")";

            $("#tournament_name").val(data.name);
            $("#tournamentname_date").val("");
            $(".givendate").text(data.start_date + " To " + data.end_date);
            sessionStorage.startdate = data.start_date;
            sessionStorage.enddate = data.end_date;
            $("#tournament_time").val(data.time);
            $("#tournament_maxentries").val(data.max_entries);
            $("#tournament_sportsname").val(data.sport.name);

            $("#tournament_stadname").val(data.stadium.name);
            $("#tournament_stadcontactno").val(data.stadium.contact_number);
            sessionStorage.stadiumid = data.stadium.id;

            $("#tournament_street").val(data.address.street);
            sessionStorage.addressid = data.address.id;
            $("#tournament_area").val(data.address.area.name);
            $("#tournament_city").val(data.address.city);
            $("#tournament_state").val(data.address.state);
            $("#tournament_country").val(data.address.country);
            $("#tournament_zipcode").val(data.address.zipcode);
            $("#tournament_lat").val(data.address.latitude);
            $("#tournament_long").val(data.address.longitude);
            $("#tournament_landmark").val(data.address.landmark);

            $(".dyn_rulesli").empty();
            for (var i = 0; i < data.rules.length; i++) {
                $(".dyn_rulesli").append(`<div class="col-md-12">
                                    <div class="contactaddress">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <p><b>Rule :</b> ${data.rules[i].rules} </p>
                                                <p><b>Category :</b> <span>${data.rules[i].category}</span></p>
                                            </div>
                                            <div class="col-md-1"><a class="pointer addressanchor" data-toggle="modal" data-target="#editrulesmodal" onclick="getrulesdetails(${i} , ${data.rules[i].id})"><i class="fa fa-pencil" aria-hidden="true"></i></a></div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>`);
            } //for loop ends here

            $(".dynpriceamountli").empty();
            for (var i = 0; i < data.prizes.length; i++) {
                $(".dynpriceamountli").append(`<li>
                                    <div class="bg-info"><a class="pointer" onclick="getpricedetails(${i} , ${data.prizes[i].id})" data-toggle="modal" data-target="#editpricingmodal"><i class="fa fa-pencil text-white"></i></a></div>
                                   ${data.prizes[i].tag} </li>`);

            } //for loop ends here

            $(".dyn_organiserli").empty();
            for (var i = 0; i < data.organizers.length; i++) {
                $(".dyn_organiserli").append(`<div class="col-md-4 brline">
                                    <div class="contactaddress">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p><b>Name :</b> ${data.organizers[i].name}</p>
                                                <p><b>Email :</b> <span>${data.organizers[i].email}</span></p>
                                                <p><b>Phone-no :</b> ${data.organizers[i].contact_number} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a data-toggle="modal" onclick="getorganiserdetails(${i} , ${data.organizers[i].id})" data-target="#editorganisermodal" class="editicon pointer"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>`);
            }

        },
        error: function(data) {
            console.log('error occured in tournament');
        }
    });
}

function readURL(input, type) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            if (type == 1) {
                var myElement = document.querySelector(".bannerimageedit");
                myElement.style.backgroundImage = "url(" + e.target.result + ")";
                // $('#blah').attr('src', e.target.result); 
            } else {
                var myElement = document.querySelector(".bannerimage-invitation");
                myElement.style.backgroundImage = "url(" + e.target.result + ")";
            }

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#coverpicture").change(function() {
    readURL(this, 1);
});

$("#invitationpicture").change(function() {
    readURL(this, 2);
});

function uploadphotos(type) {
    var postData = new FormData();

    if (type == 1) {
        if ($('#coverpicture')[0].files.length == 0) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $("#snackbarerror").text("Cover Picture is Required");
            showerrtoast();
            event.preventDefault();
            return;
        } else {
            postData.append("cover_pic", $('#coverpicture')[0].files[0]);
        }
    } else {
        if ($('#invitationpicture')[0].files.length == 0) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $("#snackbarerror").text("Invitation Picture is Required");
            showerrtoast();
            event.preventDefault();
            return;
        } else {
            postData.append("invitation_pic", $('#invitationpicture')[0].files[0]);
        }
    }

    $(".picuploadLdr_" + type).show();
    $(".picuploadLdr_" + type).attr("disabled", true);

    $.ajax({
        url: editphotos_api + sessionStorage.t_id + '/',
        type: 'put',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".picuploadLdr_" + type).hide();
            $(".picuploadLdr_" + type).attr("disabled", false);
        },
        error: function(data) {
            $(".picuploadLdr_" + type).hide();
            $(".picuploadLdr_" + type).attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Photos Updated Successfully!");
        showsuccesstoast();
        $(".photosmdlclse_" + type).click();
    }); //done fn ends here

}

var t_detailsid = ['tournament_name', 'tournament_time', 'tournament_maxentries', 'tournament_sportsname'];

var t_detailserr = ['Tournament Name', 'Tournament Time', 'Tournament Max Entries', 'Tournament Sports Name'];

// tournament details save fn starts here
function t_detailssave() {
    for (var i = 0; i < t_detailsid.length; i++) {
        if ($('#' + t_detailsid[i] + '').val() == '') {
            $('#' + t_detailsid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_detailserr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".t_detailssavldr").show();
    $(".t_detailssavBtn").attr("disabled", true);

    if ($("#tournamentname_date").val() != "") {
        var startdate = $("#tournamentname_date").val().split('-')[0];
        var enddate = $("#tournamentname_date").val().split('-')[1];
        sessionStorage.startdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
        sessionStorage.enddate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
    }

    var postData = JSON.stringify({
        "name": $("#tournament_name").val(),
        "start_date": sessionStorage.startdate.replace(/ /g, ''),
        "end_date": sessionStorage.enddate.replace(/ /g, ''),
        "time": $("#tournament_time").val(),
        "max_entries": $("#tournament_maxentries").val(),
        "sport": $("#tournament_sportsname").val()
    });

    $.ajax({
        url: edittournamnetdetails_api + sessionStorage.t_id + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".t_detailssavldr").hide();
            $(".t_detailssavBtn").attr("disabled", false);
            $(".givendate").text(sessionStorage.startdate.replace(/ /g, '') + " To " + sessionStorage.enddate.replace(/ /g, ''));
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".t_detailssavldr").hide();
            $(".t_detailssavBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });

} // tournament details save fn ends here

var t_addreid = ['tournament_street', 'tournament_area', 'tournament_city', 'tournament_state', 'tournament_country', 'tournament_zipcode', 'tournament_lat', 'tournament_long', 'tournament_landmark'];
var t_addrerr = ['Street', 'Area', 'City', 'State', 'Country', 'Zipcode', 'Latitude', 'Longitude', 'Landmark'];

var t_stadiumid = ['tournament_stadname', 'tournament_stadcontactno'];
var t_stadiumerr = ['Stadium Name', 'Stadium Contact No'];

function t_details_addr_save() {
    for (var i = 0; i < t_addreid.length; i++) {
        if ($('#' + t_addreid[i] + '').val() == '') {
            $('#' + t_addreid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_addrerr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".t_details_addr_savldr").show();
    $(".t_details_addr_savBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "street": $('#tournament_street').val(),
        "area": $('#tournament_area').val(),
        "city": $('#tournament_city').val(),
        "state": $('#tournament_state').val(),
        "country": $('#tournament_country').val(),
        "landmark": $('#tournament_landmark').val(),
        "zipcode": $('#tournament_zipcode').val(),
        "latitude": $('#tournament_lat').val(),
        "longitude": $('#tournament_long').val()
    });

    $.ajax({
        url: edittournamnet_addr_api + sessionStorage.addressid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".t_details_addr_savldr").hide();
            $(".t_details_addr_savBtn").attr("disabled", false);
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".t_details_addr_savldr").hide();
            $(".t_details_addr_savBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function t_details_stadium_save() {
    for (var i = 0; i < t_stadiumid.length; i++) {
        if ($('#' + t_stadiumid[i] + '').val() == '') {
            $('#' + t_stadiumid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_stadiumerr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    $(".t_details_stadium_savldr").show();
    $(".t_details_stadium_savBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "name": $('#tournament_stadname').val(),
        "contact_number": $('#tournament_stadcontactno').val()
    });

    $.ajax({
        url: edittournamnet_stadium_api + sessionStorage.stadiumid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".t_details_stadium_savldr").hide();
            $(".t_details_stadium_savBtn").attr("disabled", false);
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
        },
        error: function(data) {
            $(".t_details_stadium_savldr").hide();
            $(".t_details_stadium_savBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

// get rules details fn
function getrulesdetails(rowno, id) {
    sessionStorage.ruleid = id;
    var data = JSON.parse(sessionStorage.t_details);
    $("#edit_rulecategory").val(data.rules[rowno].category);
    $("#edit_ruletext").val(data.rules[rowno].rules);
}

function editrules_final() {

    if ($('#edit_ruletext').val().trim() == '') {
        $('#edit_ruletext').addClass("iserr");
        $("#snackbarerror").text("Rule is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_rulecategory').val().trim() == '') {
        $('#edit_rulecategory').addClass("iserr");
        $("#snackbarerror").text("Rule Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".edit_rules_ldr").show();
    $(".edit_rules_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "category": $('#edit_rulecategory').val(),
        "rules": $('#edit_ruletext').val()
    });

    $.ajax({
        url: editrules_api + sessionStorage.ruleid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_rules_ldr").hide();
            $(".edit_rules_Btn").attr("disabled", false);
            $("#editrulesmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_rules_ldr").hide();
            $(".edit_rules_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function editrules_final() {

    if ($('#edit_ruletext').val().trim() == '') {
        $('#edit_ruletext').addClass("iserr");
        $("#snackbarerror").text("Rule is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_rulecategory').val().trim() == '') {
        $('#edit_rulecategory').addClass("iserr");
        $("#snackbarerror").text("Rule Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".edit_rules_ldr").show();
    $(".edit_rules_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "category": $('#edit_rulecategory').val(),
        "rules": $('#edit_ruletext').val()
    });

    $.ajax({
        url: editrules_api + sessionStorage.ruleid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_rules_ldr").hide();
            $(".edit_rules_Btn").attr("disabled", false);
            $("#editrulesmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_rules_ldr").hide();
            $(".edit_rules_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addrules_final() {

    if ($('#add_ruletext').val().trim() == '') {
        $('#add_ruletext').addClass("iserr");
        $("#snackbarerror").text("Rule is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_rulecategory').val().trim() == '') {
        $('#add_rulecategory').addClass("iserr");
        $("#snackbarerror").text("Rule Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".add_rules_ldr").show();
    $(".add_rules_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "category": $('#add_rulecategory').val(),
        "rules": $('#add_ruletext').val()
    });

    $.ajax({
        url: addrules_api + sessionStorage.t_id + '/',
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_rules_ldr").hide();
            $(".add_rules_Btn").attr("disabled", false);
            $("#addrulesmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".add_rules_ldr").hide();
            $(".add_rules_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

// get price details fn
function getpricedetails(rowno, id) {
    sessionStorage.priceid = id;
    var data = JSON.parse(sessionStorage.t_details);
    $("#edit_pricetag").val(data.prizes[rowno].tag);
    $("#edit_priceamnt").val(data.prizes[rowno].amount);
    $("#edit_pricedesc").val(data.prizes[rowno].description);
}

function editprise_final() {

    if ($('#edit_pricetag').val().trim() == '') {
        $('#edit_pricetag').addClass("iserr");
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_priceamnt').val().trim() == '') {
        $('#edit_priceamnt').addClass("iserr");
        $("#snackbarerror").text("Price Amount is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_pricedesc').val().trim() == '') {
        $('#edit_pricedesc').addClass("iserr");
        $("#snackbarerror").text("Price escription is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".edit_price_ldr").show();
    $(".edit_price_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "tag": $('#edit_pricetag').val(),
        "amount": $('#edit_priceamnt').val(),
        "description": $('#edit_pricedesc').val()
    });

    $.ajax({
        url: editprice_api + sessionStorage.priceid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_price_ldr").hide();
            $(".edit_price_Btn").attr("disabled", false);
            $("#editpricingmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_price_ldr").hide();
            $(".edit_price_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addprise_final() {

    if ($('#add_pricetag').val().trim() == '') {
        $('#add_pricetag').addClass("iserr");
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_priceamnt').val().trim() == '') {
        $('#add_priceamnt').addClass("iserr");
        $("#snackbarerror").text("Price Amount is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_pricedesc').val().trim() == '') {
        $('#add_pricedesc').addClass("iserr");
        $("#snackbarerror").text("Price escription is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".add_price_ldr").show();
    $(".add_price_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "tag": $('#add_pricetag').val(),
        "amount": $('#add_priceamnt').val(),
        "description": $('#add_pricedesc').val()
    });

    $.ajax({
        url: addprice_api + sessionStorage.t_id + '/',
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_price_ldr").hide();
            $(".add_price_Btn").attr("disabled", false);
            $("#addpricingmodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".add_price_ldr").hide();
            $(".add_price_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function getorganiserdetails(rowno, id) {
    sessionStorage.organniseid = id;
    var data = JSON.parse(sessionStorage.t_details);
    $("#edit_orgname").val(data.organizers[rowno].name);
    $("#edit_orgphone").val(data.organizers[rowno].contact_number);
    $("#edit_orgemail").val(data.organizers[rowno].email);
}

function editorg_final() {

    if ($('#edit_orgname').val().trim() == '') {
        $('#edit_orgname').addClass("iserr");
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_orgphone').val().trim() == '') {
        $('#edit_orgphone').addClass("iserr");
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#edit_orgemail').val().trim() == '') {
        $('#edit_orgemail').addClass("iserr");
        $("#snackbarerror").text("Organiser Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#edit_orgemail').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#edit_orgemail').addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".edit_org_ldr").show();
    $(".edit_org_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "name": $('#edit_orgname').val(),
        "contact_number": $('#edit_orgphone').val(),
        "email": $('#edit_orgemail').val()
    });

    $.ajax({
        url: editorg_api + sessionStorage.organniseid + '/',
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".edit_org_ldr").hide();
            $(".edit_org_Btn").attr("disabled", false);
            $("#editorganisermodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".edit_org_ldr").hide();
            $(".edit_org_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function addorg_final() {

    if ($('#add_orgname').val().trim() == '') {
        $('#add_orgname').addClass("iserr");
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_orgphone').val().trim() == '') {
        $('#add_orgphone').addClass("iserr");
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#add_orgemail').val().trim() == '') {
        $('#add_orgemail').addClass("iserr");
        $("#snackbarerror").text("Organiser Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#add_orgemail').val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#add_orgemail').addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".add_org_ldr").show();
    $(".add_org_Btn").attr("disabled", true);

    var postData = JSON.stringify({
        "name": $('#add_orgname').val(),
        "contact_number": $('#add_orgphone').val(),
        "email": $('#add_orgemail').val()
    });

    $.ajax({
        url: addorg_api + sessionStorage.t_id + '/',
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".add_org_ldr").hide();
            $(".add_org_Btn").attr("disabled", false);
            $("#addorganisermodal").modal("toggle");
            $("#snackbarsuccs").text("Tournament Details Has Been Updated Successfully!");
            showsuccesstoast();
            getdetails();
        },
        error: function(data) {
            $(".add_org_ldr").hide();
            $(".add_org_Btn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}