$(function() {

    document.getElementById('coverpicture').onchange = function() {
        $(".coverfilename").text(this.value);
        $("#coverpicmodal").modal("toggle");
    };
    document.getElementById('invitationpicture').onchange = function() {
        $(".invitationfilename").text(this.value);
        $("#invitationpicmodal").modal("toggle");
    };

    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });

    $(".addtrnmntldr").hide();

});

function readURL(input, type) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            if (type == 1) {
                var myElement = document.querySelector(".bannerimage");
                myElement.style.backgroundImage = "url(" + e.target.result + ")";
                // $('#blah').attr('src', e.target.result); 
            } else {
                var myElement = document.querySelector(".bannerimage-invitation");
                myElement.style.backgroundImage = "url(" + e.target.result + ")";
            }

        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#coverpicture").change(function() {
    readURL(this, 1);
});

$("#invitationpicture").change(function() {
    readURL(this, 2);
});



//add another price fn starts here
var anotherprize = 0;

function addanotherprize() {
    if ($('#mult_pricetag_' + anotherprize).val() == '') {
        $('#mult_pricetag_' + anotherprize).addClass('iserr');
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#mult_priceamnt_' + anotherprize).val() == '') {
        $('#mult_priceamnt_' + anotherprize).addClass('iserr');
        $("#snackbarerror").text("Price Amount is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#mult_pricedesc_' + anotherprize).val() == '') {
        $('#mult_pricedesc_' + anotherprize).addClass('iserr');
        $("#snackbarerror").text("Price Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".priceaddicon" + anotherprize).empty().append(`<a onclick="removeprizerow(${anotherprize})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    anotherprize++;
    $(".pricesaddrow").append(`<div class="form-group priceformgroup priceformgroup${anotherprize}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label class="col-md-12" for="mult_pricetag_${anotherprize}">Price Tag <span class="help"></span></label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="mult_pricetag_${anotherprize}" name="example-email" class="form-control ipadd price_name" placeholder="Eg. 1st price"> </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label class="col-md-12">Price Amount</label>
                                                    <div class="col-md-12">
                                                        <input type="text" class="form-control ipadd price_amnt" id="mult_priceamnt_${anotherprize}" placeholder="Eg. Rs.5000"> </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="mt25">
                                                        <label class="col-md-12">Description</label>
                                                        <div class="col-md-12">
                                                            <textarea class="form-control resizenone ipadd price_desc" rows="2" id="mult_pricedesc_${anotherprize}"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mt25 priceaddicon${anotherprize}">
                                                        <a onclick="addanotherprize()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`);

    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });
    $(".price_amnt").keypress(function(e) {
        if ($(this).val().length > 5 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}

function removeprizerow(rowno) {
    $(".priceformgroup" + rowno).remove();
}

//add organiser fn starts here
var anotherorganiser = 0;

function addorganiser() {
    if ($('#organiser_name_' + anotherorganiser).val() == '') {
        $('#organiser_name_' + anotherorganiser).addClass('iserr');
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#organiser_phone_' + anotherorganiser).val() == '') {
        $('#organiser_phone_' + anotherorganiser).addClass('iserr');
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#organiser_email_' + anotherorganiser).val() == '') {
        $('#organiser_email_' + anotherorganiser).addClass('iserr');
        $("#snackbarerror").text("Organiser Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#organiser_email_' + anotherorganiser).val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#organiser_email_' + anotherorganiser).addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    $(".organiseraddicon" + anotherorganiser).empty().append(`<a onclick="removeorganiserrow(${anotherorganiser})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    anotherorganiser++;

    $(".addorganiserrow").append(`<div class="form-group organiserfrmgrup organiserfrmgrup${anotherorganiser}">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label class="col-md-12" for="example-email">Name <span class="help"></span></label>
                                                        <div class="col-md-12">
                                                            <input type="email" id="organiser_name_${anotherorganiser}" name="example-email" class="form-control ipadd org_name" placeholder="Enter Name"> </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="col-md-12">Phone Number</label>
                                                        <div class="col-md-12">
                                                            <input type="text" class="form-control organiserphno ipadd org_phone" id="organiser_phone_${anotherorganiser}" placeholder="Enter Phone Number"> </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <label class="col-md-12" for="example-email">Email <span class="help"></span></label>
                                                        <div class="col-md-12">
                                                            <input type="email" id="organiser_email_${anotherorganiser}" name="example-email" class="form-control ipadd org_email" placeholder="Enter email"> </div>
                                                    </div>
                                                    <div class="col-md-2 organiseraddicon${anotherorganiser}">
                                                        <a onclick="addorganiser()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                           `);
    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });
    $(".organiserphno").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}

function removeorganiserrow(rowno) {
    $(".organiserfrmgrup" + rowno).remove();
}

//addrules fn starts here
var anotherrules = 0;

function addrules() {
    if ($('#rules_name_' + anotherrules).val() == '') {
        $('#rules_name_' + anotherrules).addClass('iserr');
        $("#snackbarerror").text("Rules Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#rules_category_' + anotherrules).val() == '') {
        $('#rules_category_' + anotherrules).addClass('iserr');
        $("#snackbarerror").text("Rules Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    $(".rulesaddicon" + anotherrules).empty().append(`<a onclick="removerulesrow(${anotherrules})" class="plusicon cptr"><i class="ti-trash"></i></a>`)
    anotherrules++;
    $(".addrulesrow").append(` <div class="form-group rulesfrmgrup rulesfrmgrup${anotherrules}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label class="col-md-12">Rules & Regulations</label>
                                                    <div class="col-md-12">
                                                        <input type="text" id="rules_name_${anotherrules}" name="example-email" class="form-control rulesname ipadd" placeholder="Rules">
                                                    </div>
                                                </div>
                                                <div class="col-md-10">
                                                    <div class="mt25">
                                                        <label class="col-md-12">Category</label>
                                                        <div class="col-md-12">
                                                            <input type="text" id="rules_category_${anotherrules}" name="example-email" class="form-control rulescategory ipadd" placeholder="Category">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="mt25 rulesaddicon${anotherrules}">
                                                        <a onclick="addrules()" class="plusicon cptr"><i class="ti-plus"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>`);
    $(".ipadd").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".addtournmt_Btn").click() : "";
    });
}

function removerulesrow(rowno) {
    $(".rulesfrmgrup" + rowno).remove();
}

var t_detailsid = ['tournament_name', 'tournament_time', 'tournament_maxentries', 'tournament_sportsname', 'tournament_street', 'tournament_area', 'tournament_city', 'tournament_state', 'tournament_country', 'tournament_zipcode', 'tournament_lat', 'tournament_long', 'tournament_landmark', 'tournament_stadname', 'tournament_stadcontactno'];

var t_detailserr = ['Tournament Name', 'Tournament Time', 'Tournament Max Entries', 'Tournament Sports Name', 'Street', 'Area', 'City', 'State', 'Country', 'Zipcode', 'Latitude', 'Longitude', 'Landmark', 'Stadium Name', 'Stadium Contact No'];


//add tournament services fn starts here
function addtournament_fn() {

    var postData = new FormData();

    if ($('#coverpicture')[0].files.length == 0) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#snackbarerror").text("Cover Picture is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#invitationpicture')[0].files.length == 0) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#snackbarerror").text("Invitation Picture is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    //tournament details validation , //address details validation

    for (var i = 0; i < t_detailsid.length; i++) {
        if ($('#' + t_detailsid[i] + '').val() == '') {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#' + t_detailsid[i] + '').addClass("iserr");
            $("#snackbarerror").text("" + t_detailserr[i] + " is required");
            showerrtoast();
            event.stopPropagation();
            return;
        }
    }

    //organiser details validation
    if ($('#organiser_name_' + anotherorganiser).val() == '') {
        $('#organiser_name_' + anotherorganiser).addClass('iserr');
        $("#snackbarerror").text("Organiser Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#organiser_phone_' + anotherorganiser).val() == '') {
        $('#organiser_phone_' + anotherorganiser).addClass('iserr');
        $("#snackbarerror").text("Organiser Phone No is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#organiser_email_' + anotherorganiser).val() == '') {
        $('#organiser_email_' + anotherorganiser).addClass('iserr');
        $("#snackbarerror").text("Organiser Email ID is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    var x = $('#organiser_email_' + anotherorganiser).val();
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
        $('#organiser_email_' + anotherorganiser).addClass("iserr");
        $("#snackbarerror").text("Valid E-Mail Address is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //rules validations
    if ($('#rules_name_' + anotherrules).val() == '') {
        $('#rules_name_' + anotherrules).addClass('iserr');
        $("#snackbarerror").text("Rules Name is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#rules_category_' + anotherrules).val() == '') {
        $('#rules_category_' + anotherrules).addClass('iserr');
        $("#snackbarerror").text("Rules Category is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    //price tag validation
    if ($('#mult_pricetag_' + anotherprize).val() == '') {
        $('#mult_pricetag_' + anotherprize).addClass('iserr');
        $("#snackbarerror").text("Price Tag is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#mult_priceamnt_' + anotherprize).val() == '') {
        $('#mult_priceamnt_' + anotherprize).addClass('iserr');
        $("#snackbarerror").text("Price Amount is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }
    if ($('#mult_pricedesc_' + anotherprize).val() == '') {
        $('#mult_pricedesc_' + anotherprize).addClass('iserr');
        $("#snackbarerror").text("Price Description is Required");
        showerrtoast();
        event.preventDefault();
        return;
    }

    $(".addtrnmntldr").show();
    $(".addtrnmntBtn").attr("disabled", true);

    postData.append("cover_pic", $('#coverpicture')[0].files[0]);
    postData.append("invitation_pic", $('#invitationpicture')[0].files[0]);

    postData.append("name", $('#tournament_name').val());
    var startdate = $("#tournamentname_date").val().split('-')[0];
    var enddate = $("#tournamentname_date").val().split('-')[1];
    startdate = startdate.split("/")[2] + '-' + startdate.split("/")[0] + '-' + startdate.split("/")[1];
    enddate = enddate.split("/")[2] + '-' + enddate.split("/")[0] + '-' + enddate.split("/")[1];
    postData.append("start_date", startdate.replace(/ /g, ''));
    postData.append("end_date", enddate.replace(/ /g, ''));
    postData.append("time", $('#tournament_time').val());
    postData.append("max_entries", $('#tournament_maxentries').val());
    postData.append("sport", $('#tournament_sportsname').val());

    var address = {
        "street": $('#tournament_street').val(),
        "area": $('#tournament_area').val(),
        "city": $('#tournament_city').val(),
        "state": $('#tournament_state').val(),
        "country": $('#tournament_country').val(),
        "landmark": $('#tournament_landmark').val(),
        "zipcode": $('#tournament_zipcode').val(),
        "latitude": $('#tournament_lat').val(),
        "longitude": $('#tournament_long').val()
    }
    postData.append("address", JSON.stringify(address));

    var stadium = { "name": $('#tournament_stadname').val(), "contact_number": $('#tournament_stadcontactno').val() }
    postData.append("stadium", JSON.stringify(stadium));

    var prizes = [];
    for (var i = 0; i < $(".priceformgroup").length; i++) {
        prizes.push({ "tag": $(".price_name").eq(i).val(), "amount": $(".price_amnt").eq(i).val(), "description": $(".price_desc").eq(i).val() });
    }
    postData.append("prizes", JSON.stringify(prizes));

    var organizers = [];
    for (var i = 0; i < $(".organiserfrmgrup").length; i++) {
        organizers.push({ "name": $(".org_name").eq(i).val(), "contact_number": $(".org_phone").eq(i).val(), "email": $(".org_email").eq(i).val() });
    }
    postData.append("organizers", JSON.stringify(organizers));

    var rules = [];
    for (var i = 0; i < $(".rulesfrmgrup").length; i++) {
        rules.push({ "category": $(".rulescategory").eq(i).val(), "rules": $(".rulesname").eq(i).val() });
    }
    postData.append("rules", JSON.stringify(rules));

    $.ajax({
        url: createtouranment_api,
        type: 'post',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $(".addtrnmntldr").hide();
            $(".addtrnmntBtn").attr("disabled", false);
        },
        error: function(data) {
            $(".addtrnmntldr").hide();
            $(".addtrnmntBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Tournament Has Been Created Successfully!");
        showsuccesstoast();
        // setTimeout(function() { location.reload(); }, 3000);

    }); //done fn ends here

}