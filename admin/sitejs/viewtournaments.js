$(function() {

    $(".ldmoreldr,.ldmorebtn").hide();
    listingtournaments(0);

});

var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];

function listingtournaments(type) {

    if (type == 0) {
        $('.dyn_listtournaments').empty();
        var url = listtournaments_api;
    } else {
        var url = localStorage.nexturl;
        $(".ldmoreldr").show();
    }

    $.ajax({
        url: url,
        type: 'POST',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(response) {
            $('.ldmoreldr').hide();

            if (response.next_url) {
                $('.ldmorebtn').show();
                localStorage.nexturl = response.next_url;
            } else {
                $('.ldmorebtn').hide();
            }
            if (response.results.length == 0) {
                $('.dyn_ideaslists').append(`<div class="col-md-12"><center><img src="images/nodata.png"></center></div>`);
            } else {
                for (var i = 0; i < response['results'].length; i++) {
                    //           {
                    //     "id": 2,
                    //     "name": "SanSpo 001",
                    //     "address": {
                    //         "id": 2,
                    //         "street": "Yass Villa",
                    //         "area": {
                    //             "id": 3,
                    //             "name": "Royapettah"
                    //         },
                    //         "city": "Kanchipuram",
                    //         "state": "Tamil Nadu",
                    //         "country": "India",
                    //         "zipcode": "600035",
                    //         "latitude": "13.033816",
                    //         "longitude": "80.246958",
                    //         "landmark": "Near Santhome Church"
                    //     },
                    //     "start_date": "2017-11-10",
                    //     "end_date": "2017-11-20",
                    //     "time": "8.00 AM to 5.00 PM",
                    //     "max_entries": 5,
                    //     "cover_pic": "http://192.168.10.249:8002/media/cover_pic/sports1_d8VKyqx.jpg"
                    // }
                    var data = response['results'][i];

                    var startdate = monthNames[parseInt(data.start_date.substring(5, 7)) - 1] + " " + data.start_date.substring(0, 4) + ", " + data.start_date.substring(8, 10);
                    var enddate = monthNames[parseInt(data.end_date.substring(5, 7)) - 1] + " " + data.end_date.substring(0, 4) + ", " + data.end_date.substring(8, 10);

                    $(".dyn_listtournaments").append(`<div class="col-sm-6">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="addserviceheading">${data.name}</p>
                                </div>
                                <div class="col-sm-6">
                                    <a class="savebtn cptr" onclick="gotoedit(${data.id})">Edit</a>
                                    <a class="viewmorebtn cptr" onclick="gotoview(${data.id})">View More</a>
                                </div>
                            </div>
                            <hr class="mt-0 mb-10">
                            <div class="row">
                                <div class="col-md-3">
                                <a class="thumbnail fancybox" rel="gallery1" href="${data.cover_pic}">
                                    <img src="${data.cover_pic}" class="img-responsive" style="height:80px; !important">
                                    </a>
                                </div>
                                <div class="col-md-9">
                                    <ul class="pl-0 mb-0">
                                        <li class="distancelabel">
                                            <span class="distancecount">${data.address.area.name}</span>
                                        </li>
                                        <li class="reviewslabel">
                                            <span class="reviewscount">${data.address.landmark}</span>
                                        </li>
                                    </ul>
                                    <p class="sportsclubname">${data.name}</p>
                                    <p class="sportslocation">${data.address.street}, ${data.address.area.name}, ${data.address.city}, ${data.address.state}, ${data.address.country}, ${data.address.zipcode}.</p>
                                    <a class="viewinmap cptr" onclick="showmapwindow(${data.address.latitude},${data.address.longitude})">View in Map</a>
                                </div>
                            </div>
                            <hr class="mtb-10">
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="detailstitle">Date:</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="detailscontent">From ${startdate} To ${enddate}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="detailstitle">Time:</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="detailscontent">${data.time}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <p class="detailstitle">Maximum Entries:</p>
                                </div>
                                <div class="col-md-9">
                                    <p class="detailscontent">
                                        ${data.max_entries}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>`)
                } //for loop ends here

            } //else cond ends here
            if (type == 1) {
                $(".ldmoreldr").hide();
            }

            if ($('.lightbox-image').length) {
                $('.lightbox-image').fancybox({
                    openEffect: 'elastic',
                    closeEffect: 'elastic',
                    helpers: {
                        media: {}
                    }
                });
            }

            if ($('.fancybox').length) {
                $('.fancybox').fancybox({
                    padding: 0
                });
            }

        },
        error: function(edata) {
            console.log("error occured in load dyn_data page");
            $(".dyn_listtournaments").empty();
            $(".ldmoreldr,.ldmorebtn").hide();
        }
    });

}

function loaddata() {
    listingtournaments(1);
    $(".ldmoreldr").show();
}

//Scroll Handle...!!!
$(window).scroll(function() {
    if ($(window).scrollTop() == $(document).height() - $(window).height()) {
        console.log("scrolld");
        if ($(".ldmorebtn").is(":visible")) {
            listingtournaments(1);
            $(".ldmoreldr").show();
        }
    }
});

function gotoedit(tournamentid) {
    sessionStorage.t_id = tournamentid;
    window.location.href = "edit-tournaments.html";
}

function gotoview(tournamentid) {
    sessionStorage.t_id = tournamentid;
    window.location.href = "tournament-detail.html";
}