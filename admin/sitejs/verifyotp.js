$(function() {

    $(".dynuserphoneno").text(sessionStorage.registeredphoneno);
    $(".votpldr").hide();

    $("input").keyup(function(event) {
        $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
        event.keyCode == 13 ? $(".votpBtn").click() : "";
    });

});

//verify otp function starts here
function verifyotp_fn() {
    if ($('#usergivenotp').val().trim() == '') {
        $("#snackbarerror").text("OTP is required");
        $('#usergivenotp').addClass("iserr");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    $(".votpldr").show();
    $(".votpBtn").attr("disabled", true);
    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "mobile_number": $('.dynuserphoneno').text(),
        "otp": $('#usergivenotp').val(),
        "client": uniqueclient,
        "role": 2
    });
    $.ajax({
        url: verifyotp_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".votpldr").hide();
            $(".votpBtn").attr("disabled", false);
        },
        error: function(data) {
            $(".votpldr").hide();
            $(".votpBtn").attr("disabled", false);
            for (var key in JSON.parse(data.responseText)) { 
            	$("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    }).done(function(dataJson) {
        $("#snackbarsuccs").text("Your Accound Has Been Activated Successfully.");
        showsuccesstoast();
        localStorage.userdetails = JSON.stringify(dataJson);
        localStorage.wutkn = dataJson.token;
        localStorage.first_name = dataJson.first_name;
        localStorage.emailid = dataJson.email;
        localStorage.phoneno = dataJson.username;
        setTimeout(function() {
            window.location.href = "add-tournaments.html";
        }, 3000);
    });
} //verify otp function starts here