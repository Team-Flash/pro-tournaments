var domain = "http://192.168.1.26:8000/";
// var domain = "http://pro-tournament.zordec.com/";

//auth api
var register_api = domain + 'auth/register/';
var verifyotp_api = domain + 'auth/activate/account/';
var changephno_api = domain + 'auth/update/mobile-number/';
var login_api = domain + 'auth/login/';
var forgotpwd_api = domain + 'auth/forgot-password/';
var resetpassword_api = domain + 'auth/reset-password/';
var changepassword_api = domain + 'auth/change-password/';
var logout_api = domain + 'auth/logout/';

//add tournamnet apis 
var createtouranment_api = domain + 'tournament/create/';
var listtournaments_api = domain + 'tournament/list/';
var retrievedetails_api = domain + 'tournament/retrieve/details/';

//edit page apis
var editphotos_api = domain + 'tournament/update/images/';
var edittournamnetdetails_api = domain + 'tournament/update/';
var edittournamnet_addr_api = domain + 'tournament/update/address/';
var edittournamnet_stadium_api = domain + 'tournament/update/stadium-details/';

var editrules_api = domain + 'tournament/update/rules/';
var addrules_api = domain + 'tournament/create/rules/';

var editprice_api = domain + 'tournament/update/prizes/';
var addprice_api = domain + 'tournament/create/prizes/';

var editorg_api = domain + 'tournament/update/organizers/';
var addorg_api = domain + 'tournament/create/organizers/';

//bookings pag api
var listing_api = domain + 'booking/list/';