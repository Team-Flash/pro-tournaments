
//success toast fn starts here
function showsuccesstoast() {
    var x = document.getElementById("snackbarsuccs");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 4000);
}

//failure toast fn starts here
function showerrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 4000);
}

//error toast for ip errors
function showiperrtoast() {
    var x = document.getElementById("snackbarerror");
    x.className = "show";
    setTimeout(function() {
        x.className = x.className.replace("show", "");
    }, 3500);
}

$(function() {
    $("body").append('<center><div id="snackbarsuccs"></div><div id="snackbarerror"></div></center>');

    $("#reg_phno,#changedphno,#userphonofp,#loginphnoremailid,#rest_phno,#usergivencno,#exampltournament_contactno,.organiserphno,#tournament_stadcontactno,#edit_orgphone,#add_orgphone").keypress(function(e) {
        if ($(this).val().length > 9 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $("#exampltournament_zipcode,.price_amnt").keypress(function(e) {
        if ($(this).val().length > 5 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

     $("#tournament_maxentries").keypress(function(e) {
        if ($(this).val().length > 4 || e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $(".dyn_firstname").text(localStorage.first_name);
    $(".dyn_emailid").text(localStorage.phoneno);

    $(".logo").attr("href" , "manage-tournaments.html")
});


//logout fn starts here
function logout() {
    $.ajax({
        url: logout_api,
        type: 'post',
        headers: {
            "content-type": 'application/json'
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            sessionStorage.clear();
            localStorage.clear();

        },
        error: function(data) {
            sessionStorage.clear();
            localStorage.clear();
            window.location.replace("index.html");
        }
    }).done(function(dataJson) {
        window.location.replace("index.html");
    });
} //logout fn starts here

//map show fn starts here
function showmapwindow(lat, long, serviceid) {

    var w = window.open('', '_blank'); //you must use predefined window name here for IE.
    var head = w.document.getElementsByTagName('head')[0];

    //Give some information about the map:
    w.document.head.innerHTML = '<title>Clun Checkin | Map</title></head>';
    w.document.body.innerHTML = '<body><div id="map_canvas" style="display: block; width: 100%; height: 100%; margin: 0; padding: 0;"></div></body>';

    var loadScript = w.document.createElement('script');
    //Link to script that load google maps from hidden elements.
    loadScript.type = "text/javascript";
    loadScript.async = true;
    loadScript.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";

    var googleMapScript = w.document.createElement('script');
    var myLatLng = { lat: 13.033814, lng: 80.246957 };
    //Link to google maps js, use callback=... URL parameter to setup the calling function after google maps load.
    googleMapScript.type = "text/javascript";
    googleMapScript.async = false;
    googleMapScript.text = 'function initialize() {var mapOptions = {center: new google.maps.LatLng(' + lat + ',' + long + '),zoom: 6, mapTypeId: google.maps.MapTypeId.TERRAIN}; var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);var marker = new google.maps.Marker({position: {lat: ' + lat + ', lng: ' + long + '},map: map});}';
    head.appendChild(loadScript);
    head.appendChild(googleMapScript);
    
} //map show fn ends here
