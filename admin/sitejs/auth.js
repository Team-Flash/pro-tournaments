 $(function() {

     $(".regldr,.lgnldr,.frgtpldr,.resetpwdldr").hide();

     $(".reginput").keyup(function(event) {
         $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
         event.keyCode == 13 ? $(".regBtn").click() : "";
     });

     // $('#reg_name').val("Gopi Akshay");
     // $('#reg_phno').val("8778075612");
     // $('#reg_password').val("codehex");

     $(".lgninput").keyup(function(event) {
         $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
         event.keyCode == 13 ? $(".lgnBtn").click() : "";
     });

     // $('#loginphnoremailid').val("8610011892");
     // $('#loginpasswrd').val("codehex123");

     $(".fpinput").keyup(function(event) {
         $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
         event.keyCode == 13 ? $(".fpBtn").click() : "";
     });
     // $('#userphonofp').val("8610011892");

     $(".rspwdinput").keyup(function(event) {
         $(this).val() ? $(this).removeClass('iserr') : $(this).addClass('iserr');
         event.keyCode == 13 ? $(".resetpwdBtn").click() : "";
     });

 });

 function register() {

     if ($('#reg_name').val().trim() == '') {
         $('#reg_name').addClass("iserr");
         $("#snackbarerror").text("Name is Required");
         showerrtoast();
         event.preventDefault();
         return;
     }

     if ($('#reg_phno').val().trim() == '') {
         $('#reg_phno').addClass("iserr");
         $("#snackbarerror").text("Phone Number is Required");
         showerrtoast();
         event.preventDefault();
         return;
     }
     if ($('#reg_password').val().trim() == '') {
         $('#reg_password').addClass("iserr");
         $("#snackbarerror").text("Password is Required");
         showerrtoast();
         event.preventDefault();
         return;
     }
     if ($('#reg_password').val().length < 6) {
         $('#reg_password').addClass("iserr");
         $("#snackbarerror").text("Min 6 Characters is Required for Password!");
         showerrtoast();
         event.preventDefault();
         return;
     }

     $(".regldr").show();
     $(".regBtn").attr("disabled", true);

     var postData = JSON.stringify({
         "first_name": $('#reg_name').val(),
         "username": $('#reg_phno').val(),
         "password": $('#reg_password').val(),
         "userprofile": {
             "role": 2
         }
     });

     $.ajax({
         url: register_api,
         type: 'post',
         data: postData,
         headers: {
             "content-type": 'application/json',
         },
         success: function(data) {
             $(".regldr").hide();
             $(".regBtn").attr("disabled", false);
             $("#snackbarsuccs").text("Your accound has been created successfully. Plase verify your phone number to activate your account!");
             showsuccesstoast();
             sessionStorage.registeredphoneno = $("#reg_phno").val();
             sessionStorage.registereduserid = data.id;
             setTimeout(function() {
                 window.location.href = "enter-otp.html";
             }, 3000);
         },
         error: function(data) {
             $(".regldr").hide();
             $(".regBtn").attr("disabled", false);
             for (var key in JSON.parse(data.responseText)) {
                 $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
             }
             showerrtoast();
         }
     });
 } //register fn ends here

 // login fn starts here
 function login() {
     if ($('#loginphnoremailid').val().trim() == '') {
         $("#snackbarerror").text("Phone No is required");
         $('#loginphnoremailid').addClass("iserr");
         showiperrtoast();
         event.stopPropagation();
         return;
     }
     var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
     var str = $('#loginphnoremailid').val();
     if (numberRegex.test(str)) {
         var phone = $('#loginphnoremailid').val();
         var phoneNum = phone.replace(/[^\d]/g, '');
         if (phoneNum.length < 10 || phoneNum.length > 11) {
             $('#loginphnoremailid').addClass("iserr");
             $("#snackbarerror").text("Valid Phone No is required");
             showiperrtoast();
             event.stopPropagation();
             return;
         }
     } else {
         var x = $('#loginphnoremailid').val();
         var atpos = x.indexOf("@");
         var dotpos = x.lastIndexOf(".");
         if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
             $('#loginphnoremailid').addClass("iserr");
             $("#snackbarerror").text("Valid E-Mail Address is required");
             showiperrtoast();
             event.stopPropagation();
             return;
         }
     }
     if ($('#loginpasswrd').val().trim() == '') {
         $("#snackbarerror").text("Password is required");
         $('#loginpasswrd').addClass("iserr");
         showiperrtoast();
         event.stopPropagation();
         return;
     }
     $(".lgnldr").show();
     $(".lgnBtn").attr("disabled", true);
     var uniqueclient = Math.random() * 10000000000000000;
     var postData = JSON.stringify({
         "username": $('#loginphnoremailid').val(),
         "password": $('#loginpasswrd').val(),
         "client": uniqueclient,
         "role": 2
     });
     $.ajax({
         url: login_api,
         type: 'post',
         data: postData,
         headers: {
             "content-type": 'application/json',
         },
         success: function(data) {
             $(".lgnldr").hide();
             $(".lgnBtn").attr("disabled", false);
         },
         error: function(data) {
             $(".lgnldr").hide();
             $(".lgnBtn").attr("disabled", false);
             for (var key in JSON.parse(data.responseText)) {
                 $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
             }
             showerrtoast();
         }
     }).done(function(dataJson) {

         localStorage.userdetails = JSON.stringify(dataJson);
         localStorage.wutkn = dataJson.token;
         localStorage.first_name = dataJson.first_name;
         localStorage.emailid = dataJson.email;
         localStorage.phoneno = dataJson.username;
         window.location.href = "add-tournaments.html";

     }); //done fn ends here
 } //login fn ends here

 //forgot password fn starts here
 function forgotpwd() {

     if ($('#userphonofp').val().trim() == '') {
         $('#userphonofp').addClass("iserr");
         $("#snackbarerror").text("Phone No is required");
         showerrtoast();
         event.stopPropagation();
         return;
     }
     $(".frgtpldr").show();
     $(".fpBtn").attr("disabled", true);
     var uniqueclient = Math.random() * 10000000000000000;
     var postData = JSON.stringify({
         "mobile_number": $('#userphonofp').val(),
         "role": 2
     });
     $.ajax({
         url: forgotpwd_api,
         type: 'post',
         data: postData,
         headers: {
             "content-type": 'application/json',
         },
         success: function(data) {
             $(".frgtpldr").hide();
             $(".fpBtn").attr("disabled", false);
         },
         error: function(data) {
             $(".frgtpldr").hide();
             $(".fpBtn").attr("disabled", false);
             for (var key in JSON.parse(data.responseText)) {
                 $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
             }
             showerrtoast();
         }
     }).done(function(dataJson) {
         $("#snackbarsuccs").text("OTP has been sent to " + $('#userphonofp').val());
         showsuccesstoast();
         sessionStorage.forgotphoneno = $("#userphonofp").val();
         setTimeout(function() {
             window.location.href = "reset-password.html";
         }, 1000);
     }); //done fn ends here

 } //forgot password fn ends here


 //reset password fn starts here
 function restpwd() {
     if ($('#rsotp').val().trim() == '') {
         $('#rsotp').addClass("iserr");
         $("#snackbarerror").text("OTP is required");
         showerrtoast();
         event.stopPropagation();
         return;
     }
     if ($('#password').val().trim() == '') {
         $('#password').addClass("iserr");
         $("#snackbarerror").text("Password is Required");
         showerrtoast();
         event.preventDefault();
         return;
     }
     if ($('#password').val().length < 6) {
         $('#password').addClass("iserr");
         $("#snackbarerror").text("Min 6 Characters is Required for Password!");
         showerrtoast();
         event.preventDefault();
         return;
     }
     if ($('#retype_password').val().trim() == '') {
         $('#retype_password').addClass("iserr");
         $("#snackbarerror").text("Re-enter password is required");
         showerrtoast();
         event.preventDefault();
         return;
     }
     if ($('#retype_password').val().length < 6) {
         $('#retype_password').addClass("iserr");
         $("#snackbarerror").text("Min 6 Characters is Required for Re-Enter Password!");
         showerrtoast();
         event.preventDefault();
         return;
     }
     if ($('#retype_password').val() != $('#password').val()) {
         $("#snackbarerror").text("Passwords Do Not Match!");
         showerrtoast();
         event.preventDefault();
         return;
     }

     $(".resetpwdldr").show();
     $(".resetpwdBtn").attr("disabled", true);

     var uniqueclient = Math.random() * 10000000000000000;
     var postData = JSON.stringify({
         "mobile_number": sessionStorage.forgotphoneno,
         "client": uniqueclient,
         "otp": $('#rsotp').val(),
         "password": $('#password').val(),
         "role": 2
     });
     $.ajax({
         url: resetpassword_api,
         type: 'post',
         data: postData,
         headers: {
             "content-type": 'application/json',
         },
         success: function(data) {
             $(".resetpwdldr").hide();
             $(".resetpwdBtn").attr("disabled", false);
         },
         error: function(data) {
             $(".resetpwdldr").hide();
             $(".resetpwdBtn").attr("disabled", false);
             for (var key in JSON.parse(data.responseText)) {
                 $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
             }
             showerrtoast();
         }
     }).done(function(dataJson) {
         $("#snackbarsuccs").text("Password has been set successfully");
         showsuccesstoast();
         localStorage.userdetails = JSON.stringify(dataJson);
         localStorage.wutkn = dataJson.token;
         localStorage.first_name = dataJson.first_name;
         localStorage.emailid = dataJson.email;
         localStorage.phoneno = dataJson.username;
         setTimeout(function() {
             window.location.href = "add-tournaments.html";
         }, 3000);
     }); //done fn ends here
 } //reset password fn ends here